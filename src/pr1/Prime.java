package pr1;
import java.util.ArrayList;
import java.util.List;

public class Prime {
	private List<Integer> primeList=new ArrayList<Integer>();//nr prime intre 1 si marginea superioara a intervalului
	private List<Integer> primeNr=new ArrayList<Integer>();//numarul de nr prime din suma
	private List<Integer> primeSum=new ArrayList<Integer>();//nr prime a caror suma e din nr prime
	
	public boolean IsPrime(int nr)
	{
		int max=(int)nr/2;
		if(nr ==1 || nr==0)
			return false;
		if(nr==2)
			return true;
		for(int i=2;i<=max;++i)
		{
			if(nr%i==0)
				return false;
		}
		return true;
	}
	
	public int LargestPrimeNr(int a,int b)
	{
		
		if(b>=2)
		{
			int suma=0;
			int count,k,min=0;
			for(int i=0;i<=b;++i)
			{
				if(IsPrime(i))
				{
					primeList.add(i);
				}
			}
			//primul numar prim din interval
			for(int i=a;i<=b;++i)
			{
				if(IsPrime(i))
				{
					min=i;
					break;
				}
			}
			for(int i=primeList.indexOf(min);i<primeList.size();++i){
				k=0;
				for(int j=0;j<=i;++j)
				{
					k=j;
					suma=0;
					count=0;
					while(suma<primeList.get(i))
					{
						suma+=primeList.get(k);
						k++;
						count++;
					}
					if(suma==primeList.get(i))
					{
						primeSum.add(suma);
						primeNr.add(count);
						break;
					}
				}
			}
		}
			
			else
			{
				System.out.println("Nu exista nr prime in acest interval");
				System.exit(0);
			}
		int max=primeNr.get(0);
		int index=0;
		for(int i=0;i<primeNr.size();++i)
		{
			if(max<=primeNr.get(i)){
				max=primeNr.get(i);
				index=i;
			}
			
		}
		
	return primeSum.get(index);
	
	}
	public List<Integer> getPrimeSum(int index) {
		return primeSum;
	}

	public List<Integer> getPrimeNr() {
		return primeNr;
	}

} 
